# Stat4 validation echo switch

This is an echo switch as described in the paper. We test the function of Stat4 by ingesting packets with a ethertype of `0x88b5`, that contain no further headers beyond the 802.3 Ethernet header, and a single 4-byte integer `x`. We increment the frequency of `x` within the switch, and return a similar packet with 5 4-byte integers corresponding to `N`, `Xsum`, `Xsum_sq`, `VarNX`, `StdNX`, and `Median`. This is verified against values computed by `numpy`, and the run is halted if a mismatch is found.

## Running the example

Simply run `sudo ./run.sh`. This produces a file `bench.log` in the same directory which contains verbose results of the run.
