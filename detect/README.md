# Packet count anomaly detection

In this example, we demonstrate the use of Stat4 in two distinct dimensions: firstly, we use Stat4 with a time-based sliding window to detect anomalous packet volumes. When an anomaly is detected, we instantiate a second Stat4 counter with a single bin for each /24 prefix, to identify a prefix receiving a disproportionate amount of the traffic. Finally, the second counter is reconfigured so that each host within the prefix is allocated a bin, and the destination of the problematic traffic found. The anomaly is induced through a script that generates packets at a set rate as a baseline, and then randomly picks a destination to send flood traffic.

## Running the example
Two terminals are required to run this example - I prefer the use of `tmux`.

In one terminal, run `sudo ./run.sh`. This compiles the P4 program, and sets up the topology. The script will prompt you to start the controller. When this happens, run `sudo python controller.py` in the second terminal to instantiate the P4 controller.

Once the Mininet CLI is up, running `H0 python traffic_generator.py flood` will select a host at random within the network (and print its address), and begin flooding it with packets. During this time, the controller will print output corresponding to the state of Stat4 running on the virtual P4 switch, and eventually isolate the destination of the flood. At this point, you can verify that the destination address matches the address displayed in the terminal with the Mininet CLI.

## Notes
The mininet topology seems to periodically produce bursts of traffic unrelated to our experiments. The P4 application notices the traffic bursts and sends an alert to the controller, which in turn installs new rules in the switch to delve into the potential anomaly.
In practice, this means that you may see periodic notification of refinements for anomaly detection logged by both the controller and the switch, which however do not result in any follow-up.
This background activity does not hamper the ability of the application to pinpoint the IP of the host flooded with packets when running `H0 python traffic_generator.py flood`.
