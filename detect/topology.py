#!/usr/bin/env python

# ================
# Author: Sam Gao
# Year:   2021
# ================

import sys
sys.path.insert(0 , '../util/')

from mininet.topo import Topo
from mininet.net import Mininet
from mininet.log import setLogLevel
from mininet.node import OVSKernelSwitch
from mininet.cli import CLI
from p4_mininet import P4Switch

from config import SUBNET_COUNT, HOST_COUNT

import signal
import threading
import os
import time

setLogLevel('info')
nodes = []
THRIFT_PORT = 22222

class SimpleTopo(Topo):
    def __init__(self):
        super(SimpleTopo, self).__init__()

        router = self.addSwitch('S0', sw_path='/usr/bin/simple_switch',
            json_path='p4src/detect.json', thrift_port=THRIFT_PORT, log_console=False)

        host = self.addNode('H0', ip='10.0.0.1/8')
        self.addLink(router, host, bw=100)
        for i in range(SUBNET_COUNT):
            switch = self.addSwitch('S{}'.format(i + 1), cls=OVSKernelSwitch, protocols=['OpenFlow13'])
            for j in range(HOST_COUNT):
                inner_host = self.addNode('H{}_{}'.format(i + 1, j + 1), ip='10.0.{}.{}/8'.format(i + 1, j + 1))
                self.addLink(inner_host, switch, bw=100)
            self.addLink(switch, router, bw=100)

def main():
    net = Mininet(
        topo=SimpleTopo(), 
        switch=P4Switch, 
        controller = None,
        autoSetMacs = True
    )

    net.start()

    for i in range(SUBNET_COUNT):
        os.system("ovs-vsctl set-fail-mode S{} standalone".format(i + 1))

    print('Ready. Start controller.')
    time.sleep(10)

    h = net.getNodeByName('H0')
    h.popen('python traffic_generator.py all &', shell=True)

    CLI(net)

    net.stop()


if __name__ == "__main__":
    main()
