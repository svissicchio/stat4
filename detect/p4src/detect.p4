/***
 * ================
 * Author: Sam Gao
 * Year:   2021
 * ================
 ***/

// P4 application used in the detect and drill down case study described in the
// paper "Sam Gao, Mark Handley, and Stefano Vissicchio. Stats 101 in P4: Towards 
// In-Switch Anomaly Detection. Proc. HotNets, 2021."

#include <core.p4>
#include <v1model.p4>

// Needed before incuding stats_freq.p4
// We need 2 counters: 1 for top-level, and 1 for refining.
// The refine counter is capable of monitoring an entire /24, in this case.
#define STAT_FREQ_COUNTER_SIZE  256
#define STAT_FREQ_COUNTER_N     2

#include "../../stat4.p4"

// Top-level anomaly detection parameters

// In microseconds: 1048576us ~ 1sec.
// 10 windows = 10sec.
// 2-standard deviation from mean is considered anomalous.
#define BUCKET_SIZE 20
#define WINDOW_SIZE 10
#define STDEV_RANGE 2

// Counter indices.
#define COUNTER_TOPLEVEL  0
#define COUNTER_REFINE    1

// Digest types.
#define DIGEST_TYPE_LEARN 0
#define DIGEST_TYPE_TOP_LEVEL_ALERT 1
#define DIGEST_TYPE_REFINE_ALERT 2

// Other constants.
#define TYPE_IPV4 0x800
#define PROTO_TCP 6
#define PROTO_EXPERIMENTAL 253

typedef bit<9>  egressSpec_t;
typedef bit<48> macAddr_t;
typedef bit<32> ip4Addr_t;

header ethernet_t {
  macAddr_t dstAddr;
  macAddr_t srcAddr;
  bit<16>   etherType;
}

header ipv4_t {
  bit<4>      version;
  bit<4>      ihl;
  bit<8>      tos;
  bit<16>     totalLen;
  bit<16>     identification;
  bit<3>      flags;
  bit<13>     fragOffset;
  bit<8>      ttl;
  bit<8>      protocol;
  bit<16>     hdrChecksum;
  ip4Addr_t   srcAddr;
  ip4Addr_t   dstAddr;
}

header tcp_t {
  bit<16>     srcPort;
  bit<16>     dstPort;
  bit<32>     seqNo;
  bit<32>     ackNo;
  bit<4>      dataOffset;
  bit<4>      reserved;
  bit<8>      flags;
  bit<16>     window;
  bit<16>     csum;
  bit<16>     urgPtr;
}

struct learn_t {
  bit<8>  digestType;
  bit<48> srcAddr;
  bit<9>  ingress_port;
}

struct alert_top_t {
  bit<8>  digestType;
  bit<16> lastBucket;
  bit<16> N;
  bit<32> meanNX;
  bit<32> stdevNX;
}

struct alert_refine_t {
  bit<8>  digestType;
  bit<32> alertCounterIdx;
}

struct metadata {
  bit<4>          ihlRem;
  bit<4>          tcpRem;
  bit<16>         tcpLen;
  bit<32>         counter_value;
  stats_t         stats;
  learn_t         learn;
  alert_top_t     alert;
  alert_refine_t  alert_refine;
}

struct headers {
  ethernet_t              ethernet;
  ipv4_t                  ipv4;
}


/* checksum */
control MyVerifyChecksum(inout headers hdr, inout metadata meta) {
  apply {
    verify_checksum(hdr.ipv4.isValid(),
      {
        hdr.ipv4.version,
        hdr.ipv4.ihl,
        hdr.ipv4.tos,
        hdr.ipv4.totalLen,
        hdr.ipv4.identification,
        hdr.ipv4.flags,
        hdr.ipv4.fragOffset,
        hdr.ipv4.ttl,
        hdr.ipv4.protocol,
        hdr.ipv4.srcAddr,
        hdr.ipv4.dstAddr
      },
      hdr.ipv4.hdrChecksum,
      HashAlgorithm.csum16);
  }
}

control MyComputeChecksum(inout headers hdr, inout metadata meta) {
  apply {
    update_checksum(hdr.ipv4.isValid(),
      {
        hdr.ipv4.version,
        hdr.ipv4.ihl,
        hdr.ipv4.tos,
        hdr.ipv4.totalLen,
        hdr.ipv4.identification,
        hdr.ipv4.flags,
        hdr.ipv4.fragOffset,
        hdr.ipv4.ttl,
        hdr.ipv4.protocol,
        hdr.ipv4.srcAddr,
        hdr.ipv4.dstAddr
      },
      hdr.ipv4.hdrChecksum,
      HashAlgorithm.csum16);
  }
}


/* ingress */
control MyIngress(inout headers hdr,
          inout metadata meta,
          inout standard_metadata_t standard_metadata) {
  // Normal L2 learning tables
  action drop() {
    mark_to_drop(standard_metadata);
  }

  action mac_learn() {
    meta.learn.digestType = DIGEST_TYPE_LEARN;
    meta.learn.srcAddr = hdr.ethernet.srcAddr;
    meta.learn.ingress_port = standard_metadata.ingress_port;
    digest(1, meta.learn);
  }

  action forward(bit<9> egress_port) {
    standard_metadata.egress_spec = egress_port;
  }

  table source {
    key = {
      hdr.ethernet.srcAddr: exact;
    }
    actions = {
      mac_learn;
      NoAction;
    }
    size = 256;
    default_action = mac_learn;
  }

  table dest {
    key = {
      hdr.ethernet.dstAddr: exact;
    }
    actions = {
      forward;
      NoAction;
    }
    size = 256;
    default_action = NoAction;
  }

  action set_mcast_grp (bit<16> mcast_grp) {
    standard_metadata.mcast_grp = mcast_grp;
  }

  table multicast {
    key = {
      standard_metadata.ingress_port: exact;
    }
    actions = {
      set_mcast_grp;
      NoAction;
    }
    size = 256;
    default_action = NoAction;
  }

  action track_time() {
    // ?
  }

  table window_track {
    key = {
      hdr.ipv4.dstAddr: lpm;
    }
    actions = {
      track_time;
      NoAction;
    }
    size = 16;
    default_action = NoAction;
  }

  // Sets refined tracking counter value.
  action track (bit<32> counter_value) {
    meta.counter_value = counter_value;
  }

  table dest_prefix_track {
    key = {
      hdr.ipv4.dstAddr: lpm;
    }
    actions = {
      track;
      NoAction;
    }
    size = 16;
    default_action = NoAction;
  }

  // The time-based sliding window is only used for top-level search.    
  register<bit<32>>(1) next_bucket;
  register<bit<48>>(1) last_bucket_stamp;

  apply {
    source.apply();
    if (!dest.apply().hit) {
      multicast.apply();
    }
    
    // Default no-match value.
    meta.counter_value = 0xdeadbeef;
    bit<16> tmp;
    
    // Check TCP packets.
    if (hdr.ipv4.isValid() && hdr.ipv4.protocol == PROTO_EXPERIMENTAL && window_track.apply().hit) {
      dest_prefix_track.apply();

      // Top-level monitor.
      bit<48> bucket_stamp = standard_metadata.ingress_global_timestamp >> BUCKET_SIZE;
      bit<32> bucket_idx;
      next_bucket.read(bucket_idx, 0);

      bit<48> last_stamp;
      last_bucket_stamp.read(last_stamp, 0);

      if (bucket_stamp > last_stamp) {
        // Current bucket interval elapsed. Finalize the bucket and increment.
        last_bucket_stamp.write(0, bucket_stamp);

        bit<16> cval;
        read_bucket(cval, bucket_idx, COUNTER_TOPLEVEL);
        stats_get_data(meta.stats, COUNTER_TOPLEVEL);

        bit<32> nval = (bit<32>)cval * meta.stats.N;

        // Single-tail: only raise an alert if the amount of traffic is high.
        if (nval > meta.stats.Xsum + (STDEV_RANGE * meta.stats.StdNX)) { // compare Nx
          // Exceeds defined threshold.
          // Send digest so that the controller can populate the second counter.
          meta.alert.digestType = DIGEST_TYPE_TOP_LEVEL_ALERT;
          meta.alert.lastBucket = cval;
          meta.alert.N = (bit<16>)meta.stats.N;
          meta.alert.meanNX = meta.stats.Xsum;
          meta.alert.stdevNX = meta.stats.StdNX;
          digest(1, meta.alert);
        }

        // Push bucket.
        bucket_idx = bucket_idx + 1;

        // Wrap last bucket index around if window is full.
        if (bucket_idx == WINDOW_SIZE) {
          bucket_idx = 0;
        }
        
        // Ensure the new bucket is clean.
        drop_bucket(bucket_idx, COUNTER_TOPLEVEL);
        next_bucket.write(0, bucket_idx);
      }

      // Increment the bucket
      stats_push_freq(tmp, bucket_idx, COUNTER_TOPLEVEL);
    }

    if (meta.counter_value != 0xdeadbeef) {
      // Refine tracking invoked.
      stats_push_freq(tmp, meta.counter_value, COUNTER_REFINE);
      stats_get_data(meta.stats, COUNTER_REFINE);

      bit<32> nval = (bit<32>)tmp * meta.stats.N;
      // Identify the heavy flow destination.
      if (nval > meta.stats.Xsum + (STDEV_RANGE * meta.stats.StdNX)) {
        meta.alert_refine.digestType = DIGEST_TYPE_REFINE_ALERT;
        meta.alert_refine.alertCounterIdx = meta.counter_value;
        digest(1, meta.alert_refine);
      }
    }
  }
}

/* egress */
control MyEgress(inout headers hdr,
         inout metadata meta,
         inout standard_metadata_t standard_metadata) {

  apply { }
}

/* parsing */
parser MyParser(packet_in packet,
        out headers hdr,
        inout metadata meta,
        inout standard_metadata_t standard_metadata) {
  state start {
    packet.extract(hdr.ethernet);
    transition select(hdr.ethernet.etherType) {
      TYPE_IPV4: ipv4;
      default: accept;
    }
  }

  state ipv4 {
    packet.extract(hdr.ipv4);
    meta.ihlRem = hdr.ipv4.ihl - 4w5;
    meta.tcpLen = hdr.ipv4.totalLen - ((bit<16>)hdr.ipv4.ihl * 4);
    transition accept;
  }

}

control MyDeparser(packet_out packet, in headers hdr) {
  apply {
    packet.emit(hdr.ethernet);
    packet.emit(hdr.ipv4);
  }
}

/* switch v1 */
V1Switch(
  MyParser(),
  MyVerifyChecksum(),
  MyIngress(),
  MyEgress(),
  MyComputeChecksum(),
  MyDeparser()
) main;
