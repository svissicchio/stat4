#!/usr/bin/env python

# ================
# Author: Sam Gao
# Year:   2021
# ================

import argparse
import random
import time
import socket
import arpreq
from pypacker import psocket
from scapy.all import get_if_hwaddr
from scapy.layers.inet import *

from config import SUBNET_COUNT, HOST_COUNT
ALL_ADDRESSES = []
PKT = {}
LOCAL_MAC = get_if_hwaddr(conf.iface)
for i in range(SUBNET_COUNT):
    for j in range(HOST_COUNT):
        addr = "10.0.{}.{}".format(i + 1, j + 1)
        ALL_ADDRESSES.append(addr)
        mac = arpreq.arpreq(addr)
        PKT[addr] = bytes(Ether(src=LOCAL_MAC, dst=mac) / IP(dst=addr, proto=253))


parser = argparse.ArgumentParser(description="Generates meaningless traffic with protocol 253.")
parser.add_argument("mode", metavar="mode", action="store")
send_sock = psocket.SocketHndl(timeout=999999, iface_name=conf.iface)

def send_packet(addr):
    send_sock.send(PKT[addr])

args = parser.parse_args()
if args.mode == 'flood':
    # Pick a single address from all addresses, and direct traffic to only it.
    dest = random.choice(ALL_ADDRESSES)
    print("Chosen destination address is {}.".format(dest))
    while True:
        send_packet(dest)
        time.sleep(0.005)

elif args.mode == 'all':
    # Send traffic to all addresses, in a round-robin manner.
    while True:
        for d in ALL_ADDRESSES:
            send_packet(d)
            time.sleep(0.01)

else:
    print("Unknown mode '{}'.".format(args.mode))
