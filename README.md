# Welcome to the Stat4 repository!

This repository contains the source code described in the paper:
> Sam Gao, Mark Handley, Stefano Vissicchio. Stats 101 in P4: Towards In-Switch Anomaly Detection. In Proceedings of The 20th ACM Workshop on Hot Topics in Networks (HotNets). ACM, New York, NY, USA. 2021.

## Structure of the repository

This repository includes:
- `stat4.p4`: our P4 library, called Stat4, for the online computation of statistical measures of arbitrary distributions of values of interest extracted from packets. Currently, our implementation focuses on computing mean, variance, standard deviation and median of frequency distributions. It is however easy to extend the code to support other percentiles and non-frequency distributions, as also described in our paper.
- `echo`: directory including the echo application we used to validate our Stat4 implementation. Instructions on how to reproduce our validation experiments are included in the `README.md` file inside this directory.
- `detect`: directory including the case-study application described in our paper. Instructions on how to reproduce our case-study experiments are included in the `README.md` file inside this directory.
- `util`: directory with utility files.

## Trying Stat4

Although our code should work on any machine with `bmv2` and a version of the P4 language compatible with P4-16, we share the Virtual Machine (VM) we used for our experiments to avoid compatibility problems. Our VM is minimal: it is initially empty, and has no graphical interface -- but it has all is needed to play with Stat4.

_Downloading our VM_: to run the Stat4 code, you can download our VM from [this link](https://gitlab.com/svissicchio/stat4-vm).

_Setting up the VM_: import the downloaded VM in your favourite hypervisor.
We tested the Stat4 code by importing the VM in VirtualBox and providing it with 1 CPU, 2GB of RAM, 16MB of video memory, and a display scale factor of 2x on a Macbook Pro running macOS 10.
Login and password for the default user (which is among the sudoers) are both `mininet`.

From inside the VM, you can then clone this repository
> git clone https://gitlab.com/svissicchio/Stat4.git

Once the repository is cloned, just follow the instructions in the `README.md` files inside the `echo` and `detect` directories to reproduce our experiments.
